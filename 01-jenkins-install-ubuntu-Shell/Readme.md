# Installing Jenkins on Ubuntu 18 Using Shell Commands
---
## Steps:
   - Use **Vagrant** to launch a Linux Ubuntu/Bionic64 VM (See the *Vagranfile*). Specify the provisioning  shell script in the Vagrantfile. It is named (*jenkins_provisioning_ubuntu.sh*) in this example.
   - Write the Jenkins installation Shell Script. 
        
     ```sh
        #!/bin/bash 
        # Update and Upgrade all the packages
        sudo apt update -y
        sudo apt upgrade -y

        # Install Java.
        sudo apt install openjdk-8-jdk -y

        # Add the Jenkins Debian repository.
        # - First Import the GPG keys of the Jenkins repository 
        wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
        # Next, add the Jenkins repository to the system with:
        sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

        # Update the apt package list and install the latest version of Jenkins by typing:
        sudo apt update -y
        sudo apt install jenkins -y

        # Display the initial admin password
        sudo cat /var/lib/jenkins/secrets/initialAdminPassword

        # verify it by printing the service status:
        # systemctl status jenkins
     ```
 - Notes
   * Jenkins Requires Java 8. So, Java 8 should be installed. 
   * Jenkins Repository and Keys do not exist in the Default Debian Repository. So, they should be added.
     